import QtQuick 2.7
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2 as Controls
import org.kde.kirigami 2.5 as Kirigami
import org.kde.xdgdesktopportal 0.1

Kirigami.ApplicationWindow {
    id: root
    title: callback.title

    property string folder: utils.homePath
    property url fileUrl
    property url currentFile
    property var fileUrls: []
	property string acceptLabel

    FileChooserCallback {
        id: callback
    }

    pageStack.initialPage: Kirigami.ScrollablePage {
        title: root.title
        header: Controls.ToolBar {
            Row {
                Controls.ToolButton {
                    icon.name: "folder-root-symbolic"
                    height: parent.height
                    width: height
                    onClicked: dirModel.folder = "file:///"
                }

                Repeater {
                    model: utils.getUrlParts(dirModel.folder)

                    Controls.ToolButton {
                        icon.name: "arrow-right"
                        text: modelData
                        onClicked: dirModel.folder = utils.indexOfUrl(
                                       dirModel.folder, index)
                    }
                }
            }
        }
        footer: RowLayout {
            visible: !callback.selectExisting
            height: callback.selectExisting ? 0 : Kirigami.Units.gridUnit * 2
            Controls.TextField {
                Layout.fillHeight: true
                Layout.fillWidth: true
                id: fileNameField
                placeholderText: i18n("File name")
            }
            Controls.ToolButton {
                Layout.fillHeight: true
                icon.name: "dialog-ok-apply"
                onClicked: {
                    callback.accepted([dirModel.folder + "/" + fileNameField.text])
                }
            }
        }

        mainAction: Kirigami.Action {
            visible: (callback.selectMultiple || root.selectFolder) && callback.selectExisting
            text: root.acceptLabel ? root.acceptLabel : i18n("Select")
            icon.name: "object-select-symbolic"

            onTriggered: callback.accepted(root.fileUrls)
        }

        DirModel {
            id: dirModel
            folder: root.folder
            showDotFiles: false
        }

        DirModelUtils {
            id: utils
        }

        Controls.BusyIndicator {
            anchors.centerIn: parent

            width: Kirigami.Units.gridUnit * 4
            height: width

            visible: dirModel.isLoading
        }

        ListView {
            anchors.fill: parent
            model: dirModel
            clip: true

            delegate: Kirigami.BasicListItem {
                text: model.name
                icon: model.iconName
                checkable: callback.selectMultiple
                checked: root.fileUrls.includes(model.url)

                onClicked: {
                    if (model.isDir) {
                        if (root.selectFolder)
                            root.fileUrls = [model.url]

                        dirModel.folder = model.url
                    } else {
                        if (callback.selectMultiple) {
                            var index = root.fileUrls.indexOf(model.url)
                            if (index > -1)
                                // remove element
                                root.fileUrls.splice(index, 1)
                            else
                                root.fileUrls.push(model.url)
                        } else {
                            callback.accepted([model.url])
                        }
                    }
                }
            }
        }
    }
}
