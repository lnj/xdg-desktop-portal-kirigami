#include "dirmodelutils.h"
#include <QDebug>
#include <QUrl>
#include <QStandardPaths>

DirModelUtils::DirModelUtils(QObject *parent) : QObject(parent)
{
}

QStringList DirModelUtils::getUrlParts(const QUrl &url) const
{
    if (url.path() == QStringLiteral("/"))
        return QStringList();
    return url.path().split(QStringLiteral("/")).mid(1);
}

QUrl DirModelUtils::indexOfUrl(const QUrl &url, int index) const
{
    const QStringList urlParts = url.path().split(QStringLiteral("/"));
    QString path = QStringLiteral("/");
    for (int i = 0; i < index + 1; i++) {
        path += urlParts.at(i + 1);
        path += QStringLiteral("/");
    }

    qDebug() << path;
    return QUrl::fromLocalFile(path);
}

QString DirModelUtils::homePath() const
{
    QUrl url(QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    url.setScheme(QStringLiteral("file"));

    return url.toString();
}
