/*
 * Copyright © 2016-2018 Red Hat, Inc
 * Copyright © 2019 Linus Jahn <lnj@kaidan.im>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *       Jan Grulich <jgrulich@redhat.com>
 */

#include "filechooser.h"
#include "utils.h"
#include "dirmodel.h"
#include "dirmodelutils.h"

#include <QDialogButtonBox>
#include <QDBusMetaType>
#include <QDBusArgument>
#include <QLoggingCategory>
#include <QFile>
#include <QPushButton>
#include <QVBoxLayout>
#include <QUrl>
#include <QQmlEngine>
#include <QQmlContext>
#include <QApplication>

#include <KLocalizedString>
#include <KFileWidget>

Q_LOGGING_CATEGORY(XdgDesktopPortalKdeFileChooser, "xdp-kde-file-chooser")

// Keep in sync with qflatpakfiledialog from flatpak-platform-plugin
Q_DECLARE_METATYPE(FileChooserPortal::Filter)
Q_DECLARE_METATYPE(FileChooserPortal::Filters)
Q_DECLARE_METATYPE(FileChooserPortal::FilterList)
Q_DECLARE_METATYPE(FileChooserPortal::FilterListList)

QDBusArgument &operator << (QDBusArgument &arg, const FileChooserPortal::Filter &filter)
{
    arg.beginStructure();
    arg << filter.type << filter.filterString;
    arg.endStructure();
    return arg;
}

const QDBusArgument &operator >> (const QDBusArgument &arg, FileChooserPortal::Filter &filter)
{
    uint type;
    QString filterString;
    arg.beginStructure();
    arg >> type >> filterString;
    filter.type = type;
    filter.filterString = filterString;
    arg.endStructure();

    return arg;
}

QDBusArgument &operator << (QDBusArgument &arg, const FileChooserPortal::FilterList &filterList)
{
    arg.beginStructure();
    arg << filterList.userVisibleName << filterList.filters;
    arg.endStructure();
    return arg;
}

const QDBusArgument &operator >> (const QDBusArgument &arg, FileChooserPortal::FilterList &filterList)
{
    QString userVisibleName;
    FileChooserPortal::Filters filters;
    arg.beginStructure();
    arg >> userVisibleName >> filters;
    filterList.userVisibleName = userVisibleName;
    filterList.filters = filters;
    arg.endStructure();

    return arg;
}

FileDialog::FileDialog()
    : QObject()
    , m_engine(new QQmlApplicationEngine)
{
    // Register QML Types
    qmlRegisterType<DirModel>("org.kde.xdgdesktopportal", 0, 1, "DirModel");
    qmlRegisterType<DirModelUtils>("org.kde.xdgdesktopportal", 0, 1, "DirModelUtils");
    qmlRegisterType<FileChooserQmlCallback>("org.kde.xdgdesktopportal", 0, 1, "FileChooserCallback");

    m_engine->load(QStringLiteral("qrc:/FilePicker.qml"));
}

FileDialog::~FileDialog()
{
}

FileChooserQmlCallback::FileChooserQmlCallback(QObject *parent)
    : QObject(parent),
      m_selectMultiple(true),
      m_selectExisting(true),
      m_nameFilters(QStringList() << QStringLiteral("*")),
      m_mimeTypeFilters(QStringList() << QStringLiteral("*/*"))
{
}

QString FileChooserQmlCallback::title() const
{
    return m_title;
}

void FileChooserQmlCallback::setTitle(const QString &title)
{
    m_title = title;
    emit titleChanged();
}

bool FileChooserQmlCallback::selectMultiple() const
{
    return m_selectMultiple;
}

void FileChooserQmlCallback::setSelectMultiple(bool selectMultiple)
{
    m_selectMultiple = selectMultiple;
    emit selectMultipleChanged();
}

bool FileChooserQmlCallback::selectExisting() const
{
    return m_selectExisting;
}

void FileChooserQmlCallback::setSelectExisting(bool selectExisting)
{
    m_selectExisting = selectExisting;
    emit selectExistingChanged();
}

QStringList FileChooserQmlCallback::nameFilters() const
{
    return m_nameFilters;
}

void FileChooserQmlCallback::setNameFilters(const QStringList &nameFilters)
{
    m_nameFilters = nameFilters;
    emit nameFiltersChanged();
}

QStringList FileChooserQmlCallback::mimeTypeFilters() const
{
    return m_mimeTypeFilters;
}

void FileChooserQmlCallback::setMimeTypeFilters(const QStringList &mimeTypeFilters)
{
    m_mimeTypeFilters = mimeTypeFilters;
    emit mimeTypeFiltersChanged();
}

FileChooserPortal::FileChooserPortal(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
    qDBusRegisterMetaType<Filter>();
    qDBusRegisterMetaType<Filters>();
    qDBusRegisterMetaType<FilterList>();
    qDBusRegisterMetaType<FilterListList>();
}

FileChooserPortal::~FileChooserPortal()
{
}

uint FileChooserPortal::OpenFile(const QDBusObjectPath &handle,
                           const QString &app_id,
                           const QString &parent_window,
                           const QString &title,
                           const QVariantMap &options,
                           QVariantMap &results)
{
    Q_UNUSED(app_id)

    qCDebug(XdgDesktopPortalKdeFileChooser) << "OpenFile called with parameters:";
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    title: " << title;
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    options: " << options;

    bool modalDialog = true;
    bool multipleFiles = false;
    QString acceptLabel;
    QStringList nameFilters;
    QStringList mimeTypeFilters;

    /* TODO
     * choices a(ssa(ss)s)
     * List of serialized combo boxes to add to the file chooser.
     *
     * For each element, the first string is an ID that will be returned with the response, te second string is a user-visible label.
     * The a(ss) is the list of choices, each being a is an ID and a user-visible label. The final string is the initial selection,
     * or "", to let the portal decide which choice will be initially selected. None of the strings, except for the initial selection, should be empty.
     *
     * As a special case, passing an empty array for the list of choices indicates a boolean choice that is typically displayed as a check button, using "true" and "false" as the choices.
     * Example: [('encoding', 'Encoding', [('utf8', 'Unicode (UTF-8)'), ('latin15', 'Western')], 'latin15'), ('reencode', 'Reencode', [], 'false')]
     */

    if (options.contains(QStringLiteral("accept_label"))) {
        acceptLabel = options.value(QStringLiteral("accept_label")).toString();
    }

    if (options.contains(QStringLiteral("modal"))) {
        modalDialog = options.value(QStringLiteral("modal")).toBool();
    }

    if (options.contains(QStringLiteral("multiple"))) {
        multipleFiles = options.value(QStringLiteral("multiple")).toBool();
    }

    if (options.contains(QStringLiteral("filters"))) {
        FilterListList filterListList = qdbus_cast<FilterListList>(options.value(QStringLiteral("filters")));
        for (const FilterList &filterList : filterListList) {
            QStringList filterStrings;
            for (const Filter &filterStruct : filterList.filters) {
                if (filterStruct.type == 0) {
                    filterStrings << filterStruct.filterString;
                } else {
                    mimeTypeFilters << filterStruct.filterString;
                }
            }

            if (!filterStrings.isEmpty()) {
                nameFilters << QStringLiteral("%1|%2").arg(filterStrings.join(QLatin1Char(' '))).arg(filterList.userVisibleName);
            }
        }
    }

    QScopedPointer<FileDialog, QScopedPointerDeleteLater> fileDialog(new FileDialog());

    auto *callback = fileDialog->m_engine->rootObjects().first()->findChild<FileChooserQmlCallback*>({}, Qt::FindChildrenRecursively);

    callback->setTitle(title);
    callback->setSelectMultiple(multipleFiles);
    // Always true if we want to open a file
    callback->setSelectExisting(true);
    if (!nameFilters.isEmpty())
        callback->setNameFilters(nameFilters);
    if (!mimeTypeFilters.isEmpty())
        callback->setMimeTypeFilters(mimeTypeFilters);

    bool handled = false;
    connect(callback, &FileChooserQmlCallback::accepted, this, [&] (const QStringList &urls) {
        QStringList files;
        for (const auto &filename : urls)
            files << QUrl::fromLocalFile(filename).toDisplayString();

        results.insert(QStringLiteral("uris"), files);
        handled = true;
    });

    while (!handled)
        QGuiApplication::instance()->processEvents();

    qDebug() << results;

    return 1;
}

uint FileChooserPortal::SaveFile(const QDBusObjectPath &handle,
                           const QString &app_id,
                           const QString &parent_window,
                           const QString &title,
                           const QVariantMap &options,
                           QVariantMap &results)
{
    Q_UNUSED(app_id)

    qCDebug(XdgDesktopPortalKdeFileChooser) << "SaveFile called with parameters:";
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    handle: " << handle.path();
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    parent_window: " << parent_window;
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    title: " << title;
    qCDebug(XdgDesktopPortalKdeFileChooser) << "    options: " << options;

    bool modalDialog = true;
    QString acceptLabel;
    QString currentName;
    QString currentFolder;
    QString currentFile;
    QStringList nameFilters;
    QStringList mimeTypeFilters;

    // TODO parse options - choices

    if (options.contains(QStringLiteral("modal"))) {
        modalDialog = options.value(QStringLiteral("modal")).toBool();
    }

    if (options.contains(QStringLiteral("accept_label"))) {
        acceptLabel = options.value(QStringLiteral("accept_label")).toString();
    }

    if (options.contains(QStringLiteral("current_name"))) {
        currentName = options.value(QStringLiteral("current_name")).toString();
    }

    if (options.contains(QStringLiteral("current_folder"))) {
        currentFolder = QFile::decodeName(options.value(QStringLiteral("current_folder")).toByteArray());
    }

    if (options.contains(QStringLiteral("current_file"))) {
        currentFile = QFile::decodeName(options.value(QStringLiteral("current_file")).toByteArray());
    }

    if (options.contains(QStringLiteral("filters"))) {
        FilterListList filterListList = qdbus_cast<FilterListList>(options.value(QStringLiteral("filters")));
        for (const FilterList &filterList : filterListList) {
            QStringList filterStrings;
            for (const Filter &filterStruct : filterList.filters) {
                if (filterStruct.type == 0) {
                    filterStrings << filterStruct.filterString;
                } else {
                    mimeTypeFilters << filterStruct.filterString;
                }
            }

            if (!filterStrings.isEmpty()) {
                nameFilters << QStringLiteral("%1|%2").arg(filterStrings.join(QLatin1Char(' '))).arg(filterList.userVisibleName);
            }
        }
    }

    QScopedPointer<FileDialog, QScopedPointerDeleteLater> fileDialog(new FileDialog());

    // Find the created object on QML side
    // Our qml file only has one rootObject
    QObject* qmlObject = fileDialog->m_engine->rootObjects().first();

    if (!currentFolder.isEmpty()) {
        qmlObject->setProperty("folder", currentFolder);
    }

    if (!currentFile.isEmpty()) {
        qmlObject->setProperty("currentFile", currentFile);
    }

    if (!currentName.isEmpty()) {
        const QUrl url = qmlObject->property("folder").toUrl();
        qmlObject->setProperty("selected",
                               QUrl::fromLocalFile(QStringLiteral("%1/%2").arg(url.toDisplayString(QUrl::StripTrailingSlash), currentName)));
    }

    if (!acceptLabel.isEmpty()) {
        qmlObject->setProperty("acceptLabel", acceptLabel);
    }

    if (!nameFilters.isEmpty()) {
        qmlObject->setProperty("nameFilters", nameFilters);
    }

    if (!mimeTypeFilters.isEmpty()) {
        qmlObject->setProperty("mimeFilders", QStringList(mimeTypeFilters));
    }

    // TODO
    /*QObject::connect(qmlObject, SIGNAL(accepted), this, [=] {

    })*/

    /*if (fileDialog->exec() == QDialog::Accepted) {
        QStringList files;
        QUrl url = QUrl::fromLocalFile(fileDialog->m_fileWidget->selectedFile());
        files << url.toDisplayString();
        results.insert(QStringLiteral("uris"), files);
        return 0;
    }*/

    return 1;
}
